import mongoose from 'mongoose';

var Schema = mongoose.Schema({
    createdAt: {
        type: Date,
        default: Date.now
    },
    originalContractId: String,
    contractName: String,
    startDate: Date,
    endDate: Date,
    conditions: String,
    price: Number
});

export default mongoose.model('Contract', Schema);