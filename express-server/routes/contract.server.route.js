// ./express-server/routes/contract.server.route.js
import express from 'express';

//import controller file
import * as contractController from '../controllers/contract.server.controller';

// get an instance of express router
const router = express.Router();

router.route('/')
    .get(contractController.getContracts)
    .post(contractController.addContract)
    .put(contractController.updateContract);

router.route('/latest')
    .get(contractController.getLatestContracts);

router.route('/:id')
    .get(contractController.getContract);


export default router;
