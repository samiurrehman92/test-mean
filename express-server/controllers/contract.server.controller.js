// ./express-server/controllers/contract.server.controller.js
import mongoose from 'mongoose';

//import models
import Contract from '../models/contract.server.model';

export const getContracts = (req, res) => {
    Contract.find().exec((err, contracts) => {
        if (err) {
            return res.json({'success': false, 'message': 'Some Error'});
        }

        return res.json({'success': true, 'message': 'Contracts fetched successfully', contracts});
    });
};

export const getLatestContracts = (req, res) => {
    var threeDaysAgo = new Date();
    threeDaysAgo.setDate(threeDaysAgo.getDate() - 3);
    Contract.find({
        createdAt: {$gte: threeDaysAgo}
    }, [], {
        sort: {
            createdAt: -1 //Sort by Date Added DESC
        }
    }).exec((err, contracts) => {
        if (err) {
            return res.json({'success': false, 'message': 'Some Error'});
        }

        return res.json({'success': true, 'message': 'Contracts fetched successfully', contracts});
    });
};

export const addContract = (req, res) => {
    console.log(req.body);
    const newContract = new Contract(req.body);
    newContract.originalContractId = newContract._id;
    newContract.save((err, contract) => {
        if (err) {
            return res.json({'success': false, 'message': 'Some Error'});
        }

        return res.json({'success': true, 'message': 'Contract added successfully', contract});
    })
};

export const updateContract = (req, res) => {
    var contractToUpdate = req.body;
    contractToUpdate.originalContractId = contractToUpdate.originalContractId || contractToUpdate._id;
    delete contractToUpdate._id;
    const newContract = new Contract(contractToUpdate);
    newContract.save((err, contract) => {
        if (err) {
            console.log(err);
            return res.json({'success': false, 'message': 'Some Error'});
        }

        return res.json({'success': true, 'message': 'Contract version added successfully', contract});
    })
};

export const getContract = (req, res) => {
    Contract.find({_id: req.params.id}).exec((err, contract) => {
        if (err) {
            return res.json({'success': false, 'message': 'Some Error'});
        }
        if (contract.length) {
            return res.json({'success': true, 'message': 'Contract fetched by id successfully', contract});
        }
        else {
            return res.json({'success': false, 'message': 'Contract with the given id not found'});
        }
    })
};