# test MEAN app for FINLEX

1) You can run docker-compose up at the root directory

or 

2) Run the MEAN app without docker in an IDE with following steps:

2.1) run npm install in /express-server

2.2) run npm install in /angular-client

2.3) run app_hooked.js with node server

2.4) run mongodb server


NOTE: I could not finish all the tests for all components in the app 
due to time constraints, however I added enough for you to get some idea 
about my testing skills.