// ./angular-client/src/app/contract/contract-detail/contract-detail.component.ts
import {Component, OnInit} from '@angular/core';
import 'rxjs/add/operator/switchMap';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Location} from '@angular/common';

import {ContractService} from '../contract.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-contract-detail',
  templateUrl: './contract-detail.component.html',
  styleUrls: ['./contract-detail.component.css']
})
export class ContractDetailComponent implements OnInit {
  contract: any[] = [];
  oldContractVersions: any[] = [];

  constructor(
    private contractService: ContractService,
    private route: ActivatedRoute,
    private location: Location
  ) {
  }

  ngOnInit(): void {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.contractService.getContract(params.get('id')))
      .subscribe(td => {
        this.contract = td.contract[0];

        this.contractService.getLatestContracts()
          .then(contracts => {
            this.oldContractVersions = _.filter(contracts.contracts, {originalContractId: td.contract[0].originalContractId});
          })
      })

  }

  goBack(): void {
    this.location.back();
  }


}
