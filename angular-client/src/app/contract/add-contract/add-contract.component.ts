// ./angular-client/src/app/contract/contract-list/add-contract.component.ts
import {Component, OnInit} from '@angular/core';

import {Location} from '@angular/common';

import {ContractService} from '../contract.service';

import * as $ from 'jquery';

@Component({
  selector: 'app-new-contract',
  templateUrl: './add-contract.component.html',
  styleUrls: ['./add-contract.component.css']
})
export class AddContractComponent implements OnInit {
  fetchingData: boolean = false;
  apiMessage: string;
  contract: any = {};

  constructor(private contractService: ContractService,
              private location: Location
  ) {
  }

  ngOnInit(): void {
    $(document).ready(function () {
      $('#startDate').datepicker();
      $('#endDate').datepicker({
        useCurrent: false //Important! See issue #1075
      });
    });
  }

  AddContract(contract: any): void {
    if (!contract) {
      return;
    }
    this.contractService.createContract(contract)
      .then(td => {
        this.location.back();
      })
  }
}
