import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HttpModule} from '@angular/http';
import {ContractListComponent} from './contract-list.component';
import {ContractService} from '../contract.service';
import {FormsModule} from '@angular/forms';

import {RouterTestingModule} from '@angular/router/testing';


describe('ContractListComponent', () => {
  let component: ContractListComponent;
  let fixture: ComponentFixture<ContractListComponent>;
  let contractService: ContractService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContractListComponent,],
      imports: [
        RouterTestingModule,
        HttpModule,
        FormsModule
      ],
      providers: [ContractService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractListComponent);
    component = fixture.componentInstance;
    contractService = fixture.debugElement.injector.get(ContractService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Test of showUpdateContract', function () {
    it('should do nothing if called without correct params', function () {
      component.apiMessage = 'test';
      expect(component.showUpdateContract(undefined)).toBeFalsy();
      expect(component.apiMessage).toEqual('test');
    })

    it('should set contractToEdit with the provided contract and reset apiMessage', function () {
      component.apiMessage = 'test';
      expect(component.showUpdateContract({
        _id: 'test'
      })).toBeFalsy();
      expect(component.apiMessage).toEqual('');
      expect(component.contractToEdit).toEqual({
        _id: 'test'
      });
    })
  })

  describe('Test of UpdateContract', function () {
    beforeEach(function () {
      spyOn(contractService, 'updateContract').and.returnValue(Promise.resolve({message: 'apiResponse'}));
    })

    it('should do nothing if called without correct params', function () {
      expect(component.UpdateContract(undefined)).toBeFalsy();
      expect(contractService.updateContract).not.toHaveBeenCalled();
    })

    it('should set contractToEdit with the provided contract and reset apiMessage', function () {
      component.UpdateContract({
        _id: 'test'
      });
      //expect(component.apiMessage).toEqual('apiResponse');
      expect(contractService.updateContract).toHaveBeenCalledWith({
        _id: 'test',
        createdAt: jasmine.any(Date)
      })
    })
  })

});
