// ./angular-client/src/app/contract/contract-list/add-contract.component.ts
import {Component, OnInit} from '@angular/core';

import {ContractService} from '../contract.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-contract-list',
  templateUrl: './contract-list.component.html',
  styleUrls: ['./contract-list.component.css']
})
export class ContractListComponent implements OnInit {
  contracts: any[] = [];
  contract: any = {};
  contractToEdit: any = {};
  fetchingData: boolean = false;
  apiMessage: string;

  constructor(private contractService: ContractService) {
  }

  ngOnInit(): void {
    this.contractService.getContracts()
      .then(td => this.contracts = _.uniqBy(td.contracts.reverse(), 'originalContractId'))
  }

  showUpdateContract(contract: any): void {
    if (!contract){
      return;
    }
    this.contractToEdit = contract;
    this.apiMessage = "";
  }

  UpdateContract(contract: any): void {
    if (!contract) {
      return;
    }
    contract.createdAt = new Date();
    this.contractService.updateContract(contract)
      .then(td => {
        this.contracts.push(td.contract);
        this.contracts = _.uniqBy(this.contracts, 'originalContractId');
        this.apiMessage = td.message;
      })
  }
}
