// ./angular-client/src/app/contract/contract-routing/contract-routing.module.ts
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ContractListComponent} from '../contract-list/contract-list.component';
import {ContractDetailComponent} from '../contract-detail/contract-detail.component';
import {AddContractComponent} from '../add-contract/add-contract.component';

const contractRoutes: Routes = [
  {path: 'add-contract', component: AddContractComponent},
  {path: 'contracts', component: ContractListComponent},
  {path: 'contract/:id', component: ContractDetailComponent}
]

@NgModule({
  imports: [
    RouterModule.forChild(contractRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class ContractRoutingModule {
}
