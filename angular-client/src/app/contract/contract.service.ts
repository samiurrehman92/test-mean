// ./angular-client/src/app/contract/contract.service.ts
import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ContractService {
  private apiUrl = 'http://localhost:3001/contracts/';

  constructor(private http: Http) {
  }

  getContracts(): Promise<any> {
    return this.http.get(this.apiUrl)
      .toPromise()
      .then(this.handleData)
      .catch(this.handleError)
  }

  getLatestContracts(): Promise<any> {
    return this.http.get(this.apiUrl + 'latest')
      .toPromise()
      .then(this.handleData)
      .catch(this.handleError)
  }

  getContract(id: string): Promise<any> {
    return this.http.get(this.apiUrl + id)
      .toPromise()
      .then(this.handleData)
      .catch(this.handleError)
  }

  createContract(contract: any): Promise<any> {
    return this.http.post(this.apiUrl, contract)
      .toPromise()
      .then(this.handleData)
      .catch(this.handleError)
  }

  updateContract(contract: any): Promise<any> {
    return this.http
      .put(this.apiUrl, contract)
      .toPromise()
      .then(this.handleData)
      .catch(this.handleData);
  }

  private handleData(res: any) {
    let body = res.json();
    console.log(body); // for development purposes only
    return body || {};
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for development purposes only
    return Promise.reject(error.message || error);
  }

}
