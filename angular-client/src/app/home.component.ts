// ./angular-client/src/app/home.component.ts
import {Component, OnInit} from '@angular/core';

import {ContractService} from './contract/contract.service';
import * as _ from 'lodash';

@Component({
  selector: 'my-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomePageComponent implements OnInit {
  contracts: any[] = [];

  constructor(private contractService: ContractService) {
  }

  ngOnInit(): void {
    this.contractService.getLatestContracts()
      .then(contracts => {
        this.contracts = _.uniqBy(contracts.contracts, 'originalContractId');
      })
  }

}
