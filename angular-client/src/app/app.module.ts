// ./angular-client/src/app/app.module.ts
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {HomePageComponent} from './home.component';

import {AppRoutingModule} from './app-routing.module';
import {ContractService} from "./contract/contract.service";
import {ContractRoutingModule} from './contract/contract-routing/contract-routing.module';
import {ContractListComponent} from './contract/contract-list/contract-list.component';
import {ContractDetailComponent} from './contract/contract-detail/contract-detail.component';
import {AddContractComponent} from './contract/add-contract/add-contract.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    AddContractComponent,
    ContractListComponent,
    ContractDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    ContractRoutingModule,
    FormsModule
  ],
  providers: [ContractService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
